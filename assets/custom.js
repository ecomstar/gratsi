var isCartOpen = false;
var isProcessing = false;

$(document).ready(function(){

  klaviyo();

  sliderInit();

  miniCart();

  productPageInit();

  productRecharInit();

  ajaxATC();

  cartDraw();

  layout();

})

function klaviyo() {
  
}

function sliderInit() {
  if($(window).width() < 990) {
    
    $("[data-mob-slider]").slick({
      arrows: false,
      dots: true
    })

  }
}

function layout() {
  $(document).on('click', '[data-bg]', function() {
    isCartOpen = false;
    closeCart();
  })
}

function cartDraw(){
  var cartItemsHtml = '';
  $.ajax({
    type: 'GET',
    url: '/cart.js',
    dataType: 'json',
    success: function(cart){
      var cartItems = cart['items'];
      console.log(cart);

      if(cartItems.length > 0) {
        $('.minicart-empty').attr('data--hidden', 'true');
        $('.minicart-content').removeAttr('data--hidden');
      }
      else {
        $('.minicart-empty').removeAttr('data--hidden');
        $('.minicart-content').attr('data--hidden', 'true');
      }

      $("[data-cart-item-count]").html(cart['item_count']);

      for(var i = 0; i < cartItems.length; i ++) {
        var itemHtml = '';
        var item = cartItems[i];
        var isPlan = false;
        var planId = '';
        if(item['selling_plan_allocation']) {
          planId = item['selling_plan_allocation']['selling_plan']['id'];
          isPlan = true;
        }
        itemHtml += '<div class="minicart-item" data-id="' + item['variant_id'] + '"' + (isPlan ? ('data-plan-id="' + planId + '"') : '') + '>';
          itemHtml += '<div class="item-wrapper">';
            itemHtml += '<div class="image-wrapper"><a href="' + item['url'] + '"><img src="' + item['image'] + '"></a></div>';
            itemHtml += '<div class="item-info">';
              itemHtml += '<div class="item-title">';
                itemHtml += '<div class="title">' + item['title'] + '<span>1 set of 2</span>' + '</div>';
                itemHtml += '<div class="item-price">' + formatMoney(item['price']) +'</div>';
              itemHtml += '</div>';
              itemHtml += '<div class="item-qty">';
                itemHtml += '<div class="qty-wrapper">';
                  itemHtml += '<button data-cart-qty-control data-qty-minus><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.75 12H20.25" stroke="#221F1F" stroke-width="1.5" stroke-linecap="square"/></svg></button>';
                  itemHtml += '<div data-cart-qty>' + item['quantity'] + '</div>';
                  itemHtml += '<button data-cart-qty-control data-qty-plus><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.75 12H20.25" stroke="#221F1F" stroke-width="1.5" stroke-linecap="square"/><path d="M12 3.75V20.25" stroke="#221F1F" stroke-width="1.5" stroke-linecap="square"/></svg></button>';
                itemHtml += '</div>';
                itemHtml += '<div class="item-remove"><a href="javascript:;" data-item-remove>REMOVE</a></div>';
              itemHtml += '</div>';
            itemHtml += '</div>';
          itemHtml += '</div>';
        itemHtml += '</div>';

        cartItemsHtml += itemHtml;
      }
      $("#minicart .minicart-items").html(cartItemsHtml);
      $("[minicart-total-price]").html(formatMoney(cart['total_price']));
      if(isCartOpen) {
        openCart();
      }
    }
  })
}

function formatMoney(price){
  return '$' + (price / 100.0).toFixed(2);
}


function productRecharInit(){
  var timer = setTimeout(function(){
    if($('[data-widget-container-wrapper]').length == 0) {
      productRecharInit();
    }
    else {
      isAtcAvailable = true;
      $(".payment-buttons-container").show();
      $("[data-ajax-atc][data-atc-regular] span").text($(".rc_widget__price.rc_widget__price--onetime").html());
      $("[data-ajax-atc][data-atc-subscribe] span").text($(".rc_widget__price.rc_widget__price--subsave").html());
    }
  }, 500);
}


function ajaxATC() {
  $(document).on('click', '[data-ajax-atc]', function(){
    var handle = $(this).attr('data-type');
    if(handle == 'regular') {
      $("label.rc_widget__option__label[data-label-onetime]").click();
    }
    else {
      $("label.rc_widget__option__label[data-label-subsave]").click();
    }
    var data = $("form#product-form-page-product").serialize();
    console.log(data);
    $.ajax({
      type: 'post',
      url: '/cart/add.js',
      data: data,
      dataType: 'json',
      success: function(){
        isCartOpen = true;
        cartDraw();
      }
    })
  })
}


function productPageInit() {
  $(document).on('click', '[data-qty-control][data-qty-minus]', function(){
    var $qty = $(this).closest('.qty-box').find("#quantity");
    var qty = parseInt($qty.val());
    if(qty > 1) {
      qty -= 1;
      $qty.val(qty);
      $(this).closest('.qty-box').find('.qty-text span').text(qty);
    }
  })
  $(document).on('click', '[data-qty-control][data-qty-plus]', function(){
    var $qty = $(this).closest('.qty-box').find("#quantity");
    var qty = parseInt($qty.val());
    qty += 1;
    $qty.val(qty);
    $(this).closest('.qty-box').find('.qty-text span').text(qty);
  })
  $(document).on('mouseover', '[data-ajax-atc][data-atc-subscribe]', function(){
    $(this).find('[data-popup]').addClass('active');
  })
  $(document).on('mouseleave', '[data-ajax-atc][data-atc-subscribe]', function(){
    $(this).find('[data-popup]').removeClass('active');
  })

}


function miniCart() {
  $(document).on('click', '.minicart-item [data-qty-minus]', function(){
    var $item = $(this).closest('.minicart-item');
    var qty = parseInt($(this).closest('.qty-wrapper').find('[data-cart-qty]').text());
    qty -= 1;
    var id = $item.attr('data-id');
    var query = 'updates[' + id + ']=' + qty;
    // if($item.attr('data-plan-id')) {
    //   query += '&selling_plan=' + $item.attr('data-plan-id');
    // }
    updateCart(query);
  })

  $(document).on('click', '.minicart-item [data-qty-plus]', function(){
    var $item = $(this).closest('.minicart-item');
    var qty = parseInt($(this).closest('.qty-wrapper').find('[data-cart-qty]').text());
    qty += 1;
    var id = $item.attr('data-id');
    var query = 'updates[' + id + ']=' + qty;
    // if($item.attr('data-plan-id')) {
    //   query += '&selling_plan=' + $item.attr('data-plan-id');
    // }
    updateCart(query);
  })

  $(document).on('click', '.minicart-item [data-item-remove]', function(){
    var id = $(this).closest('.minicart-item').attr('data-id');
    var query = 'updates[' + id + ']=0';
    updateCart(query);
  })

  $(document).on('click', '[data-minicart-trigger]', function(){
    isCartOpen = true;
    openCart();
  })

  $(document).on('click', '[data-cart-close]', function() {
    isCartOpen = false;
    closeCart();
  })

  $(document).on('click', '[data-note-trigger]', function() {
    $(this).closest('.cart_note').find('textarea').show();
    $(this).closest('.label').hide();
  })

}
//selling_plan=16285880&form_type=product&id=37822980096184&quantity=10

function updateCart(query) {
  jQuery.post(
    '/cart/update.js', 
    query,
    function() {
      cartDraw();
    }
  );
}

function openCart() {
  $("body").addClass('overlay').addClass('cart-open');
}

function closeCart() {
  $("body").removeClass('overlay').removeClass('cart-open');
}